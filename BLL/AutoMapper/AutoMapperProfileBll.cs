﻿using AutoMapper;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(BLL.AutoMapperProfileBll), "CreateMaps")]
namespace BLL
{
    public static class AutoMapperProfileBll
    {                
        public static void CreateMaps()
        {
            Mapper.CreateMap<Blog, BlogDTO>();
            Mapper.CreateMap<BlogDTO, Blog>();


            Mapper.CreateMap<Post, PostDTO>();
            Mapper.CreateMap<PostDTO, Post>();

        }
    }
}
