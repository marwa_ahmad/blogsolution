﻿using AutoMapper;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class PostBLL
    {
        private PostRepository _postRepository;
        public PostBLL()
        {
            _postRepository = new PostRepository();
        }

        public void CreatePost(PostDTO post)
        {
            var postModel = Mapper.Map<Post>(post);
            _postRepository.Insert(postModel);
        }

        public IEnumerable<PostDTO> GetPostsbyBlog(int blogId)
        {            
            var postsModel = _postRepository.GetPostsByBlogId(blogId);
            return Mapper.Map<IEnumerable<PostDTO>>(postsModel);
        }
    }
}
