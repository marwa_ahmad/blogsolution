﻿using AutoMapper;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class BlogBLL
    {
        private BlogRepository _blogRepository;
        public BlogBLL()
        {
            _blogRepository = new BlogRepository();
        }

        public void CreateBlog(BlogDTO blog)
        {
            var blogModel = Mapper.Map<Blog>(blog);
            _blogRepository.Insert(blogModel);
        }

        public IEnumerable<BlogDTO> GetAllBlogs()
        {
            var blogsModel = _blogRepository.GetAll();
            return Mapper.Map<IEnumerable<BlogDTO>>(blogsModel);
        }        
    }
}
