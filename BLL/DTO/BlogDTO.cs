﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class BlogDTO
    {
        public int BlogId { get; set; }
        public string Title { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
