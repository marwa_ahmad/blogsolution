﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public class PostRepository :BaseRepository<Post>
    {
        public IEnumerable<Post> GetPostsByBlogId(int blogId)
        {
            var posts = (from p in base.DB.Posts
                        where p.BlogId == blogId
                        select p).ToList();
            return posts;
        }

    }
}
