﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace DAL
{
    /// <summary>
    /// reference: http://www.c-sharpcorner.com/UploadFile/8a67c0/Asp-Net-mvc-code-first-approach-with-repository-pattern/
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BaseRepository<T> : IRepository<T> where T : class
    {
        protected BlogDBContext DB;  
        protected DbSet<T> DbSet;

        public BaseRepository()
        {  
            DB = new BlogDBContext();  
            DbSet = DB.Set<T>();  
        }  
        public IEnumerable<T> GetAll()  
        {  
            return DbSet.ToList();  
        }  
         
        public T GetById(object Id)
        {  
            return DbSet.Find(Id);  
        }  
  
        public void Insert(T obj)  
        {  
            DbSet.Add(obj);
            DB.SaveChanges();  
        }  
        /*public void Update(T obj)  
        {  
            db.Entry(obj).State = EntityState.Modified;  
        }  
        public void Delete(object Id)  
        {  
            T getObjById = dbSet.Find(Id);  
            dbSet.Remove(getObjById);  
        } */ 
        public void Save()  
        {  
            DB.SaveChanges();  
        }  
        protected virtual void Dispose(bool disposing)  
        {  
            if (disposing)  
            {  
                if (this.DB != null)
                {  
                    this.DB.Dispose();  
                    this.DB = null;  
                }  
            }  
        }  
    }
}
