﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public class BlogRepository : BaseRepository<Blog>
    {
        
        new public void Insert(Blog blog)
        {
            blog.CreationDate = DateTime.Now;
            base.Insert(blog);
        }        
    }
}
