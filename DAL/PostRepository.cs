﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public class PostRepository : BaseRepository<Post>
    {

        public void Add(Post post)
        {
            base.Insert(post);
        }

        public IEnumerable<Post> GetAll()
        {
            return base.GetAll();
        }

        public Post GetbyId(int id)
        {
            return base.GetById(id);
        }

    }
}
