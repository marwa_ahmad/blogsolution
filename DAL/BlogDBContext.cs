﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace DAL
{
    public class BlogDBContext : DbContext
    {
        public BlogDBContext()
            : base("BlogDBConnectionString")
        {
 
        }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }
    }
}
