﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace DAL
{
    public class Blog
    {
        [Key]
        public int BlogId { get; set; }
        public string Title { get; set; }
        public DateTime CreationDate { get; set; }

        //Navigation property which enables the Lazy Loading; the contents of this property will be automatically loaded from the database when you try to access them.
        public virtual List<Post> Posts { get; set; } 
    }
}
