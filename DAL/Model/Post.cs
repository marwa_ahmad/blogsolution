﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace DAL
{
    public class Post
    {
        [Key]
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        //Matches the PK name of Blog; i.e. no need to state foriegn key
        public int BlogId { get; set; }
        public virtual Blog Blog { get; set; } 
    }
}
