using System.Web.Mvc;
using Munq.MVC3;

[assembly: WebActivator.PreApplicationStartMethod(
	typeof(TestMVC.App_Start.MunqMvc3Startup), "PreStart")]

namespace TestMVC.App_Start {
	public static class MunqMvc3Startup {
		public static void PreStart() 
        {
			DependencyResolver.SetResolver(new MunqDependencyResolver());			
			var ioc = MunqDependencyResolver.Container;
			// Munq.Configuration.ConfigurationLoader.FindAndRegisterDependencies(ioc); // Optional

            // setup BlogController's dependencies
            ioc.Register<IDataService, DataService>();
		}
	}
}
 

