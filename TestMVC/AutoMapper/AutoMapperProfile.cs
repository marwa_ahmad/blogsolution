﻿using AutoMapper;
using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestMVC.ViewModel;

namespace TestMVC
{
    public class AutoMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMaps();
        }
        private static void CreateMaps()
        {
            Mapper.CreateMap<CreateBlogVM, BlogDTO>().ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title));
            Mapper.CreateMap<BlogDTO, GetBlogVM>();


            Mapper.CreateMap<PostDTO, GetPostVM>();
            Mapper.CreateMap<CreatePostVM, PostDTO>();

        }
    }
}