﻿using AutoMapper;
using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using TestMVC.ViewModel;


namespace TestMVC.Controllers
{
    public class PostController : Controller
    {
        public IDataService DataService { get; set; }

        public PostController(IDataService dataService)
        {
            DataService = dataService;
        }

        //
        // GET: /Post/
        public ActionResult Index(int blogId)
        {
            var posts = DataService.GetPostByBlog(blogId);
            var postsVM = Mapper.Map<IList<GetPostVM>>(posts);
            return View(postsVM);
        }

        public ActionResult Create(int blogId)
        {
            //validate the passed blogId exists in DB first
            return View();
        }

        //
        // POST: /Blog/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(int blogId, CreatePostVM model)
        {
            try
            {
                if (ModelState.IsValid == false) return View(model);

                var postDTO = Mapper.Map<PostDTO>(model);
                postDTO.BlogId = blogId;
                DataService.CreatePost(postDTO);

                return RedirectToAction("Index", "Post", new { @blogId = blogId });
            }
            catch(Exception e)
            {
                return View();
            }
        }
	}
}