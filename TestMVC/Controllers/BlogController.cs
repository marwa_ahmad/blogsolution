﻿using AutoMapper;
using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestMVC.ViewModel;

namespace TestMVC.Controllers
{
    public class BlogController : Controller
    {

        public IDataService DataService { get; set; }

        public BlogController(IDataService dataService)
        {
            DataService = dataService;
        }
        //
        // GET: /Blog/
        public ActionResult Index()
        {
            var blogs = DataService.GetAllBlogs();
            var blogsVM = Mapper.Map<IList<GetBlogVM>>(blogs);
            return View(blogsVM);
        }
        
        //
        // GET: /Blog/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Blog/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateBlogVM model)
        {
            try
            {
                if (ModelState.IsValid == false) return View(model);

                var blogDTO = Mapper.Map<BlogDTO>(model);
                DataService.CreateBlog(blogDTO);

                return RedirectToAction("Index");                
            }
            catch
            {
                return View();
            }
        }
    }
}
