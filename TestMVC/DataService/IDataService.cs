﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestMVC
{
    public interface IDataService
    {
        void CreateBlog(BlogDTO blog);
        IEnumerable<BlogDTO> GetAllBlogs();
        IEnumerable<PostDTO> GetPostByBlog(int blogId);
        void CreatePost(PostDTO post);
    }
}