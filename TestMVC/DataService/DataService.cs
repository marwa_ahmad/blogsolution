﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestMVC
{
    public class DataService : IDataService
    {
        private BlogBLL _blogBll;
        private PostBLL _postBll;

        public DataService()
        {
            _blogBll = new BlogBLL();
            _postBll = new PostBLL(); 
        }
        public void CreateBlog(BlogDTO blog)
        {
            _blogBll.CreateBlog(blog);
        }
        public IEnumerable<BlogDTO> GetAllBlogs()
        {
            return _blogBll.GetAllBlogs();
        }

        public IEnumerable<PostDTO> GetPostByBlog(int blogId)
        {
            return _postBll.GetPostsbyBlog(blogId);
        }

        public void CreatePost(PostDTO post)
        {
            _postBll.CreatePost(post);
        }
    }
}