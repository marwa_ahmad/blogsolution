﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;
using TestMVC.Utilities;

namespace TestMVC.ViewModel
{
    public class CreatePostVM
    {
        [Required(ErrorMessage = Messages.TitleRequired)]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        public string Content { get; set; }
    }
}