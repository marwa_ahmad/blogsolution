﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TestMVC.Utilities;

namespace TestMVC.ViewModel
{
    public class CreateBlogVM
    {
        [Required(ErrorMessage = Messages.TitleRequired)]
        public string Title { get; set; }
    }
}