﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestMVC.ViewModel
{
    public class SearchBlogVM
    {
        public string Title { get; set; }
        public DateTime EnterDate { get; set; }
    }
}