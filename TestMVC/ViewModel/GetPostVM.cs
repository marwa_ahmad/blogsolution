﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestMVC.ViewModel
{
    public class GetPostVM
    {
        public int BlogId { get; set; }
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}