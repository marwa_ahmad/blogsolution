﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestMVC.ViewModel
{
    public class GetBlogVM
    {
        public int BlogId { get; set; }
        public string Title { get; set; }
        public DateTime CreationDate { get; set; }
    }
}